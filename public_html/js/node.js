function Node(datos) {
    this.datos = datos[0];
    this.padre = null;
    this.costo = null;
    this.hijos = [];    

    this.setHijos = function (h) {
        if (h.length > 1) {
            for (var i = 1; i <= h.length; i++) {
                if (typeof h[i] !== 'object') {
                    
                    this.hijos.push(h[i]);
                }
                
            }
        }
    };
    this.getHijos = function () {
        return this.hijos;
    };
    this.setPadre = function (p) {
        this.padre = p;
    };
    this.getPadre = function () {
        return this.padre;
    };
    this.setDatos = function (d) {
        this.datos = d;
    };
    this.getDatos = function () {
        return this.datos;
    };
    this.setCoste = function (c) {
        this.costo = c;
    };
    this.getCoste = function () {
        return this.costo;
    };


}

//const camilo = new Persona('camilo', 22, 'masculino', ['patinar', 'bailar']);
//
//console.log(camilo)
//
//
//var Node = function () {
//    //variables globales
//    var datos;
//    var hijos = new Array();
//    var padre;
//    var costo;
//
//
//
//
//    //metodos publicos
//    return {
//        //main function to initiate the module
//        nuevo: function (datos) {
//            datos = datos[0];
//            padre = null;
//            costo = null;
//            if (datos.length > 0) {
//                for (var i = 1; i <= datos.length; i++) {
//                    hijos.push(datos[i]);
//                }
//
//            }
//
//
//        },
//        getHijos: function () {
//            return hijos;
//        },
//        setPadre: function (p) {
//            padre = p;
//        },
//        getPadre: function () {
//            return padre;
//        },
//        setDatos: function (d) {
//            datos = d;
//        },
//        getDatos: function () {
//            return datos;
//        },
//        setCoste: function (c) {
//            costo = c;
//        },
//        getCoste: function () {
//            return costo;
//        }
//    };
//
//
//}();