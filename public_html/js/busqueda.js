/* global Node */

var Busqueda = function (){
    //variables globales
    var grafo = [
            ['A', 'T', 'S', 'Z'],
            ['Z', 'O'],
            ['O', 'S'],
            ['S', 'R', 'F'],
            ['R', 'C', 'P'],
            ['C', 'P'],
            ['T', 'L'],
            ['P', 'B'],
            ['B']
        ];
        
            var costos = [
                {
                    nodo: 'A',
                    costos: [{desde: 'A', costo: 0}],
                    heuristica: 366
                },
                {
                    nodo: 'Z',
                    costos: [{desde: 'A', costo: 75}],
                    heuristica: 374
                },
                {
                    nodo: 'O',
                    costos: [{desde: 'Z', costo: 771}],
                    heuristica: 380
                },
                {
                    nodo: 'S',
                    costos: [{desde: 'A', costo: 140}, {desde: 'O', costo: 151}],
                    heuristica: 253
                },
                {
                    nodo: 'R',
                    costos: [{desde: 'S', costo: 80}],
                    heuristica: 193
                },
                 {
                    nodo: 'C',
                    costos: [{desde: 'R', costo: 146}, {desde: 'D', costo: 120}],
                    heuristica: 160
                },
                {
                    nodo: 'T',
                    costos: [{desde: 'A', costo: 118}],
                    heuristica: 329
                },
                 {
                    nodo: 'F',
                    costos: [{desde: 'S', costo: 99}],
                    heuristica: 178
                },
                {
                    nodo: 'P',
                    costos: [{desde: 'R', costo: 97}, {desde: 'C', costo: 138}],
                    heuristica: 98
                },
                {
                    nodo: 'B',
                    costos: [{desde: 'F', costo: 211}, {desde: 'P', costo: 101}],
                    heuristica: 0
                }
            
        ];
    var nodos_visitados;
    var nodos_por_visitar;
    var nodoActual;
    var nodoRaiz;
    
    var inicializacionDeComponentes = function () {
      $('#selectorAlgoritmo').change(function (e) {
            e.preventDefault();
            switch ($(this).val()) {
                case 'amplitud':
                    busquedaPorAmplitud($('#nodoObjetivo').val());
                    break;
                case 'profundidad':
                    busquedaPorProfundidad($('#nodoObjetivo').val());
                    break;
                case 'a':
                    busquedaAestrella($('#nodoObjetivo').val());
                    break;
                case 'avara':
                    busquedaAvara($('#nodoObjetivo').val());
                    break;
                    
                default:
                    
                    break;
            }
        });  
    };
    
    
    var busquedaPorAmplitud = function (objetivo, raiz) {
        nodos_visitados = new Array();
        nodos_por_visitar = new Array();
        nodoActual = new Array();        
        var cantidad = 0;
        var seEncontro = false;
        var contenedor = $('#contenedorPorAmplitud');
        
        contenedor.append('<h4>Busqueda por amplitud</h4>');
        
        while(!seEncontro ){             
            var nodo;
            if (cantidad === 0) {                
                nodo = new Node(grafo[cantidad]);
                nodo.setHijos(grafo[cantidad]);
                nodoRaiz = nodo;
                cantidad++;
            }else{
                nodo = nodos_por_visitar.shift();
                cantidad++;
            }
            contenedor.append('<p>Puntero: '+nodo.getDatos()+'</p>');
            console.log(nodo);
            if (nodo.getDatos() === objetivo) {
                seEncontro = true;
                contenedor.append('<h1><strong>Nodo solucion!</h1></p>');
                obtenerCaminoSolucion(nodo, contenedor);
            }else{
               nodos_visitados.push(nodo);
               var hijos = nodo.getHijos();
               $.each(hijos, function (k, h) {
                    $.each(grafo, function (key, g) {                        
                            if (g[0] === h) {                                
                                 var nodoHijo = new Node(g);
                                 console.log(g);
                                nodoHijo.setHijos(g);
                                nodoHijo.setPadre(nodo);
                                nodos_por_visitar.push(nodoHijo);
                                
                            }
                        }); 
                    
                    
                });
                var frontera = nodos_por_visitar.map(function (a) {
                                        return a.getDatos();
                                    });
                                contenedor.append('<p><strong>Frontera: '+frontera+'</strong></p>');
               
            }
        }
    };
    
    
        var busquedaPorProfundidad = function (objetivo) {
        nodos_visitados = new Array();
        nodos_por_visitar = new Array();
        nodoActual = new Array();        
        var cantidad = 0;
        var seEncontro = false;
        var contenedor = $('#contenedorPorProfundidad');
        
        contenedor.append('<h4>Busqueda por profundidad</h4>');
        while(!seEncontro ){ 
            console.log('Cantidad: '+cantidad);
            console.log(nodos_por_visitar.length);
            var nodo;
            if (cantidad === 0) {                
                nodo = new Node(grafo[cantidad]);
                nodo.setHijos(grafo[cantidad]);
                nodoRaiz = nodo;
                cantidad++;
            }else{
                nodo = nodos_por_visitar.pop();
                cantidad++;
            }
            contenedor.append('<p>Puntero: '+nodo.getDatos()+'</p>');
            if (nodo.getDatos() === objetivo) {
                seEncontro = true;
                contenedor.append('<h1><strong>Nodo solucion!</h1></p>');
                obtenerCaminoSolucion(nodo, contenedor);
            }else{
               nodos_visitados.push(nodo);
               var hijos = nodo.getHijos();
               $.each(hijos, function (k, h) {
                    $.each(grafo, function (key, g) {                        
                            if (g[0] === h) {
                                
                                 var nodoHijo = new Node(g);
                                 console.log(g);
                                nodoHijo.setHijos(g);
                                nodoHijo.setPadre(nodo);
                                nodos_por_visitar.push(nodoHijo);
                            }
                        }); 
                    
                    
                });
                var frontera = nodos_por_visitar.map(function (a) {
                                        return a.getDatos();
                                    });
                                contenedor.append('<p><strong>Frontera: '+frontera+'</strong></p>');
               
            }
        }
    };    
    
   
    
        var busquedaAestrella = function (objetivo, raiz) {
        nodos_visitados = new Array();
        nodos_por_visitar = new Array();
        nodoActual = new Array();        
        var cantidad = 0;
        var seEncontro = false;
        var contenedor = $('#contenedorPorAmplitud');
        
        contenedor.append('<h4>Busqueda por a estrella</h4>');
        
        while(!seEncontro ){             
            var nodo;
            if (cantidad === 0) {                
                nodo = new Node(grafo[cantidad]);
                nodo.setHijos(grafo[cantidad]);
                
                        nodo.setCoste(0);
                //alert('Costo: '+JSON.stringify(nodo));
                nodoRaiz = nodo;
                cantidad++;
            }else{
                nodo = nodos_por_visitar.pop();
                cantidad++;
            }
            contenedor.append('<p>Puntero: '+nodo.getDatos()+'</p>');
            console.log(nodo);
            if (nodo.getDatos() === objetivo) {
                seEncontro = true;
                contenedor.append('<h1><strong>Nodo solucion!</h1></p>');
                obtenerCaminoSolucion(nodo, contenedor);
            }else{
               nodos_visitados.push(nodo);
               var hijos = nodo.getHijos();
               $.each(hijos, function (k, h) {
                    $.each(grafo, function (key, g) {   
                        console.log('NODOS: '+g[0]+' === ' + h);
                            if (g[0] === h) {                                
                                 var nodoHijo = new Node(g);
                                 console.log(g);
                                nodoHijo.setHijos(g);
                                nodoHijo.setPadre(nodo); 
                                var costo = calculoFuncionEvaluacion(nodoHijo.getDatos(), nodo.getDatos(), nodo.getCoste(), costos, 'estrella');
                                
                                nodoHijo.setCoste(costo);

                            
                               // alert('Costo: '+JSON.stringify(nodoHijo));
                                nodos_por_visitar.push(nodoHijo);
                                
                            }
                        }); 
                    
                    
                });
                nodos_por_visitar.sort(function (a, b) {
                        if (a.getCoste() < b.getCoste()) {
                          return 1;
                        }
                        if (a.getCoste() > b.getCoste()) {
                          return -1;
                        }
                        // a must be equal to b
                        return 0;
                  });
                var frontera = nodos_por_visitar.map(function (a) {
                                        return (a.getDatos()+' = '+a.getCoste());
                                    });
                                contenedor.append('<p><strong>Frontera: '+frontera+'</strong></p>');
               
            }
        }
    };
    
    
    
    var busquedaAvara = function (objetivo, raiz) {
        nodos_visitados = new Array();
        nodos_por_visitar = new Array();
        nodoActual = new Array();        
        var cantidad = 0;
        var seEncontro = false;
        var contenedor = $('#contenedorPorAmplitud');
        
        contenedor.append('<h4>Busqueda por a estrella</h4>');
        
        while(!seEncontro ){             
            var nodo;
            if (cantidad === 0) {                
                nodo = new Node(grafo[cantidad]);
                nodo.setHijos(grafo[cantidad]);
                
                        nodo.setCoste(0);
                //alert('Costo: '+JSON.stringify(nodo));
                nodoRaiz = nodo;
                cantidad++;
            }else{
                nodo = nodos_por_visitar.pop();
                cantidad++;
            }
            contenedor.append('<p>Puntero: '+nodo.getDatos()+'</p>');
            console.log(nodo);
            if (nodo.getDatos() === objetivo) {
                seEncontro = true;
                contenedor.append('<h1><strong>Nodo solucion!</h1></p>');
                obtenerCaminoSolucion(nodo, contenedor);
            }else{
               nodos_visitados.push(nodo);
               var hijos = nodo.getHijos();
               $.each(hijos, function (k, h) {
                    $.each(grafo, function (key, g) {   
                        console.log('NODOS: '+g[0]+' === ' + h);
                            if (g[0] === h) {                                
                                 var nodoHijo = new Node(g);
                                 console.log(g);
                                nodoHijo.setHijos(g);
                                nodoHijo.setPadre(nodo); 
                                var costo = calculoFuncionEvaluacion(nodoHijo.getDatos(), nodo.getDatos(), nodo.getCoste(), costos, 'avara');
                                
                                nodoHijo.setCoste(costo);

                            
                               // alert('Costo: '+JSON.stringify(nodoHijo));
                                nodos_por_visitar.push(nodoHijo);
                                
                            }
                        }); 
                    
                    
                });
                nodos_por_visitar.sort(function (a, b) {
                        if (a.getCoste() < b.getCoste()) {
                          return 1;
                        }
                        if (a.getCoste() > b.getCoste()) {
                          return -1;
                        }
                        // a must be equal to b
                        return 0;
                  });
                var frontera = nodos_por_visitar.map(function (a) {
                                        return (a.getDatos()+' = '+a.getCoste());
                                    });
                                contenedor.append('<p><strong>Frontera: '+frontera+'</strong></p>');
               
            }
        }
    };
    
    
    
    var obtenerCaminoSolucion = function (nodoObjetivo, contenedor) {
        var seCompleto = false;
        var maximo = nodos_visitados.length;
        var cantidad = 0;
        var actual = nodoObjetivo;
        var camino = new Array();
        var caminoText = new Array();
        while(actual !== nodoRaiz || !seCompleto){
            camino.push(actual);
            caminoText.push(actual.getDatos());
            if (actual === nodoRaiz) {
                seCompleto = true;
            }else{
                var padre = actual.getPadre();
            $.each(nodos_visitados, function (k, n) {
                if (n === padre) {
                    actual = n;
                }
            });  
            }
                     
        }
        caminoText = caminoText.reverse();
        contenedor.append('<p><strong>Camino Solucion: '+caminoText.toString()+'</strong></p>');
        console.log('Camino '+camino);
        console.log('Camino '+caminoText.toString());
    };

    
    //metodos publicos
    return {
        //main function to initiate the module
        init: function () {
            inicializacionDeComponentes();
        }
    };
    
    
}();




